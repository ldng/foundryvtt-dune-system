export default class DuneCharacter {
  static migrate(act) {
    console.log(act);
    if (act.data.data.Traits) {
      if (act.data.data.Traits.length) {
        for (let trait of act.data.data.Traits) {
          console.log(trait);
          act.createEmbeddedDocuments(
            "Item",
            [
              {
                type: "trait",
                name: trait.name,
                data: { temporary: trait.temporary },
              },
            ],
            { renderSheet: false }
          );
        }
        act.update({ "data.Traits": null });
      } else if (act.data.data.Traits["[0]"]) {
        for (let i in act.data.data.Traits) {
          let trait = act.data.data.Traits[i];
          console.log(trait);
          act.createEmbeddedDocuments(
            "Item",
            [
              {
                type: "trait",
                name: trait.name,
                data: { temporary: trait.temporary },
              },
            ],
            { renderSheet: false }
          );
        }
        act.update({ "data.Traits": null });
      }
    }
    if (act.data.data.Assets) {
      if (act.data.data.Assets.length) {
        for (let asset of act.data.data.Assets) {
          console.log(asset);
          act.createEmbeddedDocuments(
            "Item",
            [
              {
                type: "asset",
                name: asset.name,
                data: { temporary: asset.temporary, quality: asset.quality },
              },
            ],
            { renderSheet: false }
          );
        }
        act.update({ "data.Assets": null });
      } else if (act.data.data.Assets["[0]"]) {
        for (let i in act.data.data.Assets) {
          let asset = act.data.data.Assets[i];
          console.log(asset);
          act.createEmbeddedDocuments(
            "Item",
            [
              {
                type: "asset",
                name: asset.name,
                data: { temporary: asset.temporary, quality: asset.quality },
              },
            ],
            { renderSheet: false }
          );
        }
        act.update({ "data.Assets": null });
      }
    }
    if (
      act.data.data.Skills.Battle.min == 4 &&
      act.data.data.Skills.Battle.max == 8
    ) {
      console.log("Migrate skills");
      let up = {};
      for (let skill in act.data.data.Skills) {
        up["data.Skills." + skill + ".min"] = 0;
        up["data.Skills." + skill + ".max"] = act.data.data.Skills[skill].value;
      }
      //console.log(up);
      act.update(up);
    }
  }

  static heal(act) {
    if (act == null) {
      if (canvas.tokens.controlled.length > 0)
        act = canvas.tokens.controlled[0].actor;
      else act = game.user.character;
      if (act == null)
        return "ERROR: You must pass a character, or select a token";
    }
    if (typeof act == "string") this.heal(game.actors.get(act));
    else {
      let up = {};
      for (let skill in act.data.data.Skills) {
        if (
          act.data.data.Skills[skill].value != act.data.data.Skills[skill].max
        )
          up["data.Skills." + skill + ".value"] =
            act.data.data.Skills[skill].max;
      }
      act.update(up);
    }
  }

  static healAll() {
    for (let act of game.actors.entries) {
      switch (act.data.type) {
        case "Player Character":
        case "Supporting Character":
        case "Non-Player Character":
          this.heal(act);
          break;
      }
    }
  }
}

globalThis.DuneCharacter = DuneCharacter;
