export function confirmDelete(fn, entity) {
  let t = "Delete " + entity.name;
  let c = "Are you sure you want to delete " + entity.name;
  switch (entity.type) {
    case "domain":
      c =
        "Are you sure you want to delete " +
        game.i18n.localize(
          entity.data.data.primary ? "DUNE.primary" : "DUNE.secondary"
        );
      if (entity.data.data.area) c += " " + entity.data.data.area;
      if (entity.data.data.field) c += " (" + entity.data.data.field + ")";
      if (entity.data.data.details) c += " -- " + entity.data.data.details;
      break;
  }
  let d = new Dialog({
    title: t,
    content: c,
    buttons: {
      delete: {
        icon: '<i class="fas fa-trash"></i>',
        label: game.i18n.localize("Delete"),
        callback: fn,
      },
      cancel: {
        icon: '<i class="fas fa-times"></i>',
        label: game.i18n.localize("Cancel"),
      },
    },
    default: "cancel",
  });
  d.render(true);
}

export function itemFromArray(data, id) {
  if (data.splice) {
    // is array
    let idx = id.substr(1, id.length - 2);
    return data[+idx];
  } else {
    // is ~Map
    return data[id];
  }
}

export function confirmDeleteFromArray(cb, data, id, item) {
  confirmDelete(() => {
    if (data.splice) {
      // is array
      let idx = id.substr(1, id.length - 2);
      data.splice(+idx, 1);
    } else {
      // is ~Map
      delete data[id];
      data = Object.values(data);
    }
    cb.apply(null, [data]);
  }, item || itemFromArray(data, id));
}
