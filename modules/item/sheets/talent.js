// -*- js -*-
export default class ItemSheetDuneTalent extends ItemSheet {
  /** @override */
  get template() {
    //console.log(this);
    if (!game.user.isGM && !this.options.editable)
      return "systems/dune/templates/items/limited-talent-sheet.html";
    return `systems/dune/templates/items/talent-sheet.html`;
  }

  /** @override */
  getData() {
    const data = super.getData();
    data.actor = this.actor;
    return data;
  }
}
