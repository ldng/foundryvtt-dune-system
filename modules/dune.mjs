/* -*- js -*- */
// Create global namespace
game.dune = {};

// Import Modules
import { preloadHandlebarsTemplates } from "./templates.mjs";
import ActorSheetDuneCharacter from "./actor/sheets/character.js";
import ActorSheetDuneHouse from "./actor/sheets/house.js";
import ActorSheetDuneAsset from "./actor/sheets/asset.js";
import ItemSheetDuneTalent from "./item/sheets/talent.js";
import ItemSheetDuneDomain from "./item/sheets/domain.js";
import ItemSheetDune from "./item/sheets/default.js";
import { DuneRoll } from "./dice/roll.js";
import { Pools, pools } from "./apps/pools.js";
import DuneCharacter from "./actor/character.js";

game.dune.DuneRoll = DuneRoll;
game.dune.pools = pools;

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.once("init", function () {
  console.log(`Dune | Initializing the 2d20-Dune Game System`);
  // Create a namespace within the game global
  game.dune = {};

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("dune", ActorSheetDuneCharacter, {
    types: ["Player Character", "Supporting Character", "Non-Player Character"],
    makeDefault: true,
    label: "DUNE.SheetClassCharacter",
  });
  Actors.registerSheet("dune", ActorSheetDuneHouse, {
    types: ["House"],
    makeDefault: true,
    label: "DUNE.SheetClassHouse",
  });
  Actors.registerSheet("dune", ActorSheetDuneAsset, {
    types: ["Asset"],
    makeDefault: true,
    label: "DUNE.SheetClassAsset",
  });
  Items.registerSheet("dune", ItemSheetDuneTalent, {
    types: ["talent"],
    makeDefault: true,
    label: "DUNE.Talent",
  });
  Items.registerSheet("dune", ItemSheetDune, {
    types: ["asset", "trait"],
    makeDefault: true,
    label: "DUNE.Asset",
  });

  preloadHandlebarsTemplates();
});

/* -------------------------------------------- */
/*  Migrations                                  */
/* -------------------------------------------- */
Hooks.on("renderActorDirectory", () => {
  console.log(`Dune | Checking Actors`);

  for (let act of game.actors.contents) {
    switch (act.data.type) {
      case "Player Character":
      case "Supporting Character":
      case "Non-Player Character":
        DuneCharacter.migrate(act);
        break;
    }
  }
});

Hooks.on("renderSidebarTab", async (app, html) => {
  if (app.options.id == "chat") {
    let icon = html.find(".chat-control-icon");
    icon[0].innerHTML =
      '<i id="dune-dice-roller-show" class="fas fa-dice-d20" title="Show Dune Dice Roller">';
    html.find("#dune-dice-roller-show").on("click", async (event) => {
      event.preventDefault();
      event.stopPropagation();
      DuneRoll.ui();
    });
  }
});

Handlebars.registerHelper({ not: (a) => !a });

Handlebars.registerHelper("concat", function () {
  var outStr = "";
  for (var arg in arguments) {
    if (typeof arguments[arg] != "object") {
      outStr += arguments[arg];
    }
  }
  return outStr;
});
